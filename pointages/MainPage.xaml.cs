﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace pointages
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
			this.compteur.Text = jours();

        }

        private String jours()
        {
            DateTime cible = new DateTime(2018,08,10,18,00,00); // dimanche à 8h
            DateTime jours = DateTime.Now;
            String reste = String.Concat((cible - jours).ToString(), " jours");

            return reste;
        }

        private void MainForm_Load(object sender, System.EventArgs e)
        {
            this.compteur.Text = jours();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
        // do not use
        }

        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            this.compteur.Text = jours();
        }

        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            //Close();
        }
        }
    }

