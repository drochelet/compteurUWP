﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Data.Sqlite;
using Microsoft.Data.Sqlite.Internal;


namespace pointages
{
    class sqliteWrapper
    {
        public String _query;
        static String fichier = "pointages";

        public static void configBase() // Initialise une connexion à la base et créé les tables si nécessaire
        {
            SqliteEngine.UseWinSqlite3(); //Configuring library to use SDK version of SQLite
            using (SqliteConnection db = new SqliteConnection(connectionString: $"Filename={fichier}pointages.db")) //Name of .db file doesn't matter, but should be consistent across all SqliteConnection objects
            {
                db.Open(); //Open connection to database
                String tableCommand = "CREATE TABLE IF NOT EXISTS MyTable " +
                    "(" +
                    "P_key INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "journee date NULL, " +
                    "duree float NULL, " +
                    "client varchar null, " +
                    "projet varchar null" +
                    "ticket varchar null" +
                    "pointe boolean null" +
                    ")"
                    ;
                SqliteCommand createTable = new SqliteCommand(tableCommand, db);
                try
                {
                    createTable.ExecuteReader(); //Execute command, throws SqliteException error if command doesn't execute properly
                }
                catch (SqliteException e)
                {
                    //System.Console.WriteLine(e.ToString());
                }
                db.Close();
            }
        }

        public static void writeToBase(DateTimeOffset journee, TimeSpan durée, String client, String projet, String Ticket)
        {
            String _query = "";
            using (SqliteConnection db = new SqliteConnection(connectionString: $"Filename={fichier}pointages.db"))
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                //Use parameterized query to prevent SQL injection attacks
                insertCommand.CommandText = _query;
                //insertCommand.Parameters.AddWithValue("@Entry", Input_Box.Text);

                try
                {
                    insertCommand.ExecuteReader();
                }
                catch (SqliteException e)
                {
                    //Handle error
                    return;
                }
                db.Close();
            }
        }
    }

}
